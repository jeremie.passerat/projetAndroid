package com.example.projetandroids1;

import java.io.Serializable;

/**
 * Acteurs / Techniciens d'un film (relié au film via son id)
 */

public class ActeursFilm implements Serializable {

    private int id;
    private String nom;
    private String nomPerso;
    private String image;
    private String job;

    public ActeursFilm(int id, String nom, String nomPerso, String image) {
        this.id = id;
        this.nom = nom;
        this.nomPerso = nomPerso;
        this.image = image;
    }

    // entrer un réalisateur
    public ActeursFilm(String job, int id, String nomReal, String image) {
        this.id = id;
        this.nom = nomReal;
        this.job = job;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getNomPerso() {
        return nomPerso;
    }

/* Fonctionnalité non implémentée
    public String getImage() {

        return image;
    }
*/
}
