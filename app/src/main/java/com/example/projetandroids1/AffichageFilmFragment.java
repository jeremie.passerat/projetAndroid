package com.example.projetandroids1;

import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AffichageFilmFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AffichageFilmFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AffichageFilmFragment extends Fragment {

    private LayoutInflater inflater;
    private ViewGroup container;
    private GetFilm getFilm;
    private View view;

    Context context;
    ListView listView;
    private OnFragmentInteractionListener mListener;

    public AffichageFilmFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AffichageFilmFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AffichageFilmFragment newInstance(String param1, String param2) {
        AffichageFilmFragment fragment = new AffichageFilmFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.inflater = inflater;
        this.container = container;

        this.view = inflater.inflate(R.layout.fragment_affichage_film, container, false);
        this.listView = view.findViewById(R.id.listeRecherche);

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        getFilm = new GetFilm(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void showFilm(String[] s) {

        String[] allFilms;

        // Cas ou TMDB ne renvoie rien
        if (s == null) {
            allFilms = new String[1];

            allFilms[0] = "Aucun résultat pour cette recherche";

            final ArrayAdapter<String> adapter;
            adapter = new ArrayAdapter<>(Objects.requireNonNull(context), android.R.layout.simple_list_item_1, allFilms);

            ArrayList<String> listItems = new ArrayList<>();
            listItems.addAll(Arrays.asList(allFilms));
            listView.setAdapter(adapter);

        } else {

            String[] titres = new String[s.length];
            // On récupère seulement les titres
            int j;
            for (j = 0; j < s.length; j++){
                int indexOf = s[j].indexOf("-");
                if (indexOf != -1)
                    titres[j] = s[j].substring(indexOf + 1);
            }


            ArrayList<String> listItems = new ArrayList<>();
            listItems.addAll(Arrays.asList(titres));

            final ArrayAdapter<String> adapter;
            adapter = new ArrayAdapter<>(Objects.requireNonNull(context), android.R.layout.simple_list_item_1, listItems);


            listView.setAdapter(adapter);

            // La list view permettra par la suite de charger le film
            listView.setOnItemClickListener((AdapterView<?> adapterView, View view1, int i, long l) -> {

                if (Objects.requireNonNull(adapter.getItem(i)).equals("Aucun résultat pour cette recherche")) {

                } else {

                    int indexOf = s[i].indexOf("-");
                    // On récupère l'id du film pour pouvoir le charger
                    ((MainActivity) Objects.requireNonNull(getActivity())).changeFragment(s[i].substring(0, indexOf));
                }
            });
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
