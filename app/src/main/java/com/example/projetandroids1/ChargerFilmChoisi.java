package com.example.projetandroids1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Une fois le titre du film choisi, il est extrait d'IMDB (si besoin) et mis en BD
 */

public class ChargerFilmChoisi extends AppCompatActivity {


    FilmsDBOpenHelper dbHelper;
    Film filmChoisi;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        id = getIntent().getStringExtra("IdFilm");

        dbHelper = new FilmsDBOpenHelper(this, "filmsdbNew", null, 1);

       // Toast.makeText(ChargerFilmChoisi.this, "Je recherche des infos sur :" + id, Toast.LENGTH_SHORT).show();

        new GetFilmsTask().execute(GenerateUrlSearch());
        new GetFilmsTask().execute(GenerateUrlActeurs());
        new GetFilmsTask().execute(GenerateUrlVideos());
    }

    private URL GenerateUrlActeurs() {

        String url = "https://api.themoviedb.org/3/movie/" + id + "/credits?api_key=" + getString(R.string.ApiKey);

        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private URL GenerateUrlVideos(){
        String urlVideos = "https://api.themoviedb.org/3/movie/" + id + "/videos?api_key=" + getString(R.string.ApiKey) + "&language=fr-FR";

        Log.d("Url", "Url Videos :" + urlVideos);

        try {
            return new URL(urlVideos);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public URL GenerateUrlSearch() {

        String url_base = "https://api.themoviedb.org/3/movie/"+ id +"?api_key=" + getString(R.string.ApiKey) + "&language=fr-FR";


        try {
            return new URL(url_base);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public class GetFilmsTask extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpURLConnection connection = null;
            try {
                try {
                    connection = (HttpURLConnection) params[0].openConnection();
                    int response = connection.getResponseCode();

                    // Connexion Http Ok
                    if (response == HttpURLConnection.HTTP_OK) {
                        StringBuilder builder = new StringBuilder();

                        try (BufferedReader reader = new BufferedReader(
                                new InputStreamReader(connection.getInputStream()))) {

                            String line;

                            while ((line = reader.readLine()) != null) {
                                builder.append(line);
                            }
                        } catch (IOException e) {
                            runOnUiThread(() -> Toast.makeText(ChargerFilmChoisi.this, R.string.erreurConnexionApi, Toast.LENGTH_LONG).show());
                            e.printStackTrace();
                        }

                        return new JSONObject(builder.toString());
                    } else {
                        runOnUiThread(() -> {
                            Toast.makeText(ChargerFilmChoisi.this, R.string.erreurApi, Toast.LENGTH_LONG).show();
                        });

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            } finally {
                connection.disconnect(); // fermeture HttpURLConnection
            }
            return null;
        }

        // Pendant l'execution
        @Override
        protected void onPreExecute() {
       //     runOnUiThread(() -> Toast.makeText(ChargerFilmChoisi.this, "Ca charge MDRRRRRR", Toast.LENGTH_LONG).show());
        }

        // Après l"execution
        @Override
        protected void onPostExecute(JSONObject films) {

            try {
                detailFilmChoisi(films);
            } catch (Exception e){
                try {
                    acteursFilmChoisi(films);
                } catch (Exception ex) {
                    videosFilmChoisi(films);

                }
            }
        }

    }




    public void detailFilmChoisi(JSONObject films) throws Exception {

            try {
                String id = this.id;
                String titre = films.getString("title");
                String titre_origine = films.getString("original_title");
                String budget = films.getString("budget");
                String resume = films.getString("overview");
                String dateSortie = films.getString("release_date");
                String poster = "https://image.tmdb.org/t/p/w200/" + films.getString("poster_path");
                String moyenne = films.getString("vote_average");

                filmChoisi = new Film(Integer.parseInt(id), titre, titre_origine, budget,  resume, dateSortie, poster, Double.parseDouble(moyenne));


                SQLiteDatabase db = dbHelper.getWritableDatabase();
                ContentValues toAdd = new ContentValues();

                toAdd.put(FilmsDBOpenHelper.COLUMN_ID, Integer.parseInt(id));
                toAdd.put(FilmsDBOpenHelper.COLUMN_TITRE, titre);
                toAdd.put(FilmsDBOpenHelper.COLUMN_TITREORI, titre_origine);
                toAdd.put(FilmsDBOpenHelper.COLUMN_BUDGET, budget);
                toAdd.put(FilmsDBOpenHelper.COLUMN_RESUME, resume);
                toAdd.put(FilmsDBOpenHelper.COLUMN_DATE, dateSortie);
                toAdd.put(FilmsDBOpenHelper.COLUMN_IMAGE, poster);
                toAdd.put(FilmsDBOpenHelper.COLUMN_MOYENNE, Double.parseDouble(moyenne));


                db.insert(FilmsDBOpenHelper.TABLE_FILMS, null, toAdd);
                db.close();


            } catch (JSONException e) {
                throw new Exception();
            }

    }

    public void acteursFilmChoisi(JSONObject films) throws Exception {

        try {
            JSONArray json = films.getJSONArray("cast");
            JSONArray json2 = films.getJSONArray("crew");


            int j = 0;
            while (!json2.getJSONObject(j).getString("job").equals("Director")){
                j++;
            }

            String nomReal = json2.getJSONObject(j).getString("name");
            String jobReal = json2.getJSONObject(j).getString("job");
            String imageReal = json2.getJSONObject(j).getString("profile_path");
            filmChoisi.setRealisateur(new ActeursFilm(jobReal, Integer.parseInt(id), nomReal, imageReal ));

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues toAdd = new ContentValues();

            toAdd.put(FilmsDBOpenHelper.COLUMN_ID, Integer.parseInt(id));
            toAdd.put(FilmsDBOpenHelper.COLUMN_NOM, nomReal);
            toAdd.putNull(FilmsDBOpenHelper.COLUMN_PERSO);
            toAdd.put(FilmsDBOpenHelper.COLUMN_IMAGE, imageReal);
            toAdd.put(FilmsDBOpenHelper.COLUMN_JOB, jobReal);

            db.insert(FilmsDBOpenHelper.TABLE_CASTING, null, toAdd);
            db.close();

            for (int i = 0; i < 15; i++) {

                String name = json.getJSONObject(i).getString("name");
                String image = json.getJSONObject(i).getString("profile_path");
                String personnage = json.getJSONObject(i).getString("character");

                ActeursFilm acteurFilm = new ActeursFilm(Integer.parseInt(id), name, personnage, image);
                filmChoisi.addCasting(acteurFilm);

                 db = dbHelper.getWritableDatabase();
                 toAdd = new ContentValues();

                toAdd.put(FilmsDBOpenHelper.COLUMN_ID, Integer.parseInt(id));
                toAdd.put(FilmsDBOpenHelper.COLUMN_NOM, name);
                toAdd.put(FilmsDBOpenHelper.COLUMN_PERSO, personnage);
                toAdd.put(FilmsDBOpenHelper.COLUMN_IMAGE, image);
                toAdd.putNull(FilmsDBOpenHelper.COLUMN_JOB);

                db.insert(FilmsDBOpenHelper.TABLE_CASTING, null, toAdd);
                db.close();

            }
        } catch (JSONException e) {
            throw new Exception();
        }
    }


    private void videosFilmChoisi(JSONObject films) {

    //    Toast.makeText(ChargerFilmChoisi.this, "Je cherche les vidéos", Toast.LENGTH_SHORT).show();

        try {
            JSONArray json = films.getJSONArray("results");

            for (int i = 0; i < films.length(); i++) {

                if (json.getJSONObject(i).getString("type").equals("Trailer")) {

                    String name = json.getJSONObject(i).getString("name");
                    String key = json.getJSONObject(i).getString("key");
                    String Url = "https://www.youtube.com/watch?v=" + key;

                    VideoFilm videoFilm = new VideoFilm(Integer.parseInt(id), name, Url);

                    filmChoisi.addVideo(videoFilm);

                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                    ContentValues toAdd = new ContentValues();

                    toAdd.put(FilmsDBOpenHelper.COLUMN_ID, Integer.parseInt(id));
                    toAdd.put(FilmsDBOpenHelper.COLUMN_NOM, name);
                    toAdd.put(FilmsDBOpenHelper.COLUMN_URL, Url);

                    db.insert(FilmsDBOpenHelper.TABLE_VIDEOS, null, toAdd);
                    db.close();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra("film", filmChoisi);
        setResult(Activity.RESULT_OK, intent);
        finish();


    }


}
