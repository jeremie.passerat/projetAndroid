package com.example.projetandroids1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Extraction des films liés au choix de saisie du champ de recherche
 */

public class ChargerFilmsNew extends AppCompatActivity {

    String[] listeChoixFilms;
    String champRecherche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        champRecherche = getIntent().getStringExtra("Recherche");

        Log.wtf("Erreur", "Je recherche des infos sur :" + champRecherche);

        new GetFilmsTask().execute(GenerateUrlSearch());

    }

    //Mise en forme de l'URL pour l'appel api
    public URL GenerateUrlSearch() {

        String url_base = "https://api.themoviedb.org/3/search/movie?api_key=" + getString(R.string.ApiKey) + "&langage=fr-FR&query=" + champRecherche + "&include_adult=false";

        Log.d("Url", "Url :" + url_base);

        try {
            return new URL(url_base);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public class GetFilmsTask extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpURLConnection connection = null;
            try {
                try {
                    connection = (HttpURLConnection) params[0].openConnection();
                    int response = connection.getResponseCode();

                    // Connexion Http Ok
                    if (response == HttpURLConnection.HTTP_OK) {
                        StringBuilder builder = new StringBuilder();

                        try (BufferedReader reader = new BufferedReader(
                                new InputStreamReader(connection.getInputStream()))) {

                            String line;

                            while ((line = reader.readLine()) != null) {
                                builder.append(line);
                            }
                        } catch (IOException e) {
                            runOnUiThread(() -> Toast.makeText(ChargerFilmsNew.this, R.string.erreurConnexionApi, Toast.LENGTH_LONG).show());
                            e.printStackTrace();
                        }

                        return new JSONObject(builder.toString());
                    } else {
                        runOnUiThread(() -> {
                            Toast.makeText(ChargerFilmsNew.this, R.string.erreurApi, Toast.LENGTH_LONG).show();
                        });

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            } finally {
                connection.disconnect(); // fermeture HttpURLConnection
            }
            return null;
        }

        // Pendant l'execution
        @Override
        protected void onPreExecute() {

        }

        // Après l"execution
        @Override
        protected void onPostExecute(JSONObject films) {

            listeChoixFilms(films);
        }

    }


    public void listeChoixFilms(JSONObject films) {


        try {
            if (films.getInt("total_results") > 0) {

                int total = films.getInt("total_results");
                if (total > 10){
                    total = 10;
                }

                listeChoixFilms = new String[total];


                int i;
                for (i = 0; i < total; i++) {

                    try {
                        JSONArray json = films.getJSONArray("results");
                        String titre = json.getJSONObject(i).getString("title");
                        String id = json.getJSONObject(i).getString("id");


                        String dateSortie = json.getJSONObject(i).getString("release_date");

                        if (!dateSortie.isEmpty())
                        {
                            String anneeSortie = dateSortie.substring(0, 4);
                            listeChoixFilms[i] = id + "-" + titre + " (" + anneeSortie + ")";

                        }

                        else {
                            listeChoixFilms[i] = id + "-" + titre;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        // On retourne les titres correspondants à la recherche
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra("resultat", listeChoixFilms);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}


