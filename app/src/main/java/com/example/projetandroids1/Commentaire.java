package com.example.projetandroids1;

/*
 Un commentaire, une note, un pseudo
 */

import java.io.Serializable;

class Commentaire implements Serializable {

    private int id;
    private String contenu;
    private String auteur;
    private int note;
    private String date;

    public Commentaire(int id, String contenu, String auteur, int note, String date) {
        this.id = id;
        this.contenu = contenu;
        this.auteur = auteur;
        this.note = note;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
