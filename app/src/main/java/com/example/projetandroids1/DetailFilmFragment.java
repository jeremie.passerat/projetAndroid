package com.example.projetandroids1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import java.io.InputStream;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Vector;


/**
 * Fragment d'affichage des éléments d'un film
 */

public class DetailFilmFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Film film;

    private TextView titreFilm;
    private TextView anneeFilm;
    private TextView realisateurFilm;
    private TextView titreOriFilm;
    private TextView budgetFilm;


    private View view;
    private ImageView imageView;

    private OnFragmentInteractionListener mListener;

    private FragmentResume fragmentResume;
    private FragmentCommentaires fragmentCommentaires;
    private FragmentVideos fragmentVideos;
    private FragmentCasting fragmentCasting;
    private PageAdapter2 pageAdapter2;

    private ViewPager pager;

    public DetailFilmFragment() {
    }

     public static DetailFilmFragment newInstance(Film film) {
        DetailFilmFragment fragment = new DetailFilmFragment();
        Bundle args = new Bundle();
        args.putSerializable("film", film);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            film =  (Film) getArguments().getSerializable("film");
        }

       if (film == null){
           Toast.makeText(getContext(), "Aucun film chargé", Toast.LENGTH_SHORT).show();

       }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        this.view = inflater.inflate(R.layout.fragment_detail_film, container, false);


        this.imageView = view.findViewById(R.id.imageFilm);

        this.titreFilm = view.findViewById(R.id.titreFilm);
        this.anneeFilm = view.findViewById(R.id.anneeFilm);
        this.realisateurFilm = view.findViewById(R.id.realisateurFilm);
        this.titreOriFilm = view.findViewById(R.id.titreOriFilm);
        this.budgetFilm = view.findViewById(R.id.budgetFilm);

        // Récuétation du film choisi
        assert getArguments() != null;

        this.fragmentResume = FragmentResume.newInstance(film.getResume());

        titreFilm.setText("Titre : " + film.getTitre());
        titreOriFilm.setText("Titre d'Origine : " + film.getTitre_origine());
        anneeFilm.setText("Date de Sortie : " + film.getDateSortie());
        realisateurFilm.setText("Réalisé par : " + film.getRealisateur().getNom());

        NumberFormat formatter = NumberFormat.getNumberInstance(Locale.FRANCE);

        int budgetNonFormat = Integer.parseInt(film.getBudget());

        String Budget = formatter.format(budgetNonFormat);
        budgetFilm.setText("Budget : " + Budget + "$" );


        new DownloadImageTask((ImageView) view.findViewById(R.id.imageFilm))
                .execute(film.getPoster());


         pager = view.findViewById(R.id.film_viewpager);

        StringBuilder castingTotal = new StringBuilder();

        int i;
        for (i = 0; i < film.getActeursFilms().size(); i++){
            castingTotal.append(film.getActeursFilms().get(i).getNom()).append(" - ").append(film.getActeursFilms().get(i).getNomPerso() + "\n");
        }

        //if (firstFilm) {



        fragmentResume = FragmentResume.newInstance(film.getResume());
        fragmentCasting = FragmentCasting.newInstance(film.getActeursFilms());
        fragmentVideos = FragmentVideos.newInstance(film.getVideosFilms());
        fragmentCommentaires = FragmentCommentaires.newInstance(film.getCommentaires(), film.getId());



        List<Fragment> fragments = new Vector<>();
        fragments.add(fragmentResume);
        fragments.add(fragmentCasting);
        fragments.add(fragmentVideos);
        fragments.add(fragmentCommentaires);


        pageAdapter2 = new PageAdapter2(getFragmentManager(), fragments);
        pager.setAdapter(pageAdapter2);

        //}

        TabLayout tabs= view.findViewById(R.id.film_tabs);
        tabs.setupWithViewPager(pager);
        tabs.setTabMode(TabLayout.MODE_FIXED);


        // Inflate the layout for this fragment
        return view;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }





    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
