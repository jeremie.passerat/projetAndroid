package com.example.projetandroids1;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Caracteristiques d'un film - La gestion de la moyenne a été abandonnée pour le rendu
 */

public class Film implements Serializable {

    private int id; // en int??
    private String titre;
    private String titre_origine;
    private String budget;
    private String resume;
    private String dateSortie;
    private String poster;
    private double moyenne; // int aussi ??
    private ArrayList <ActeursFilm> acteursFilms = new ArrayList<>();
    private ActeursFilm realisateur;
    private ArrayList <VideoFilm> videosFilms = new ArrayList<>();
    private ArrayList <Commentaire> commentaires = new ArrayList<>();


    public Film(int id, String titre, String titre_origine, String budget, String resume, String dateSortie, String poster, double moyenne) {
        this.id = id;
        this.titre = titre;
        this.titre_origine = titre_origine;
        this.budget = budget;
        this.resume = resume;
        this.dateSortie = dateSortie;
        this.poster = poster;
        this.moyenne = moyenne;
    }

    public void addCasting (ActeursFilm acteursFilm){
        this.acteursFilms.add(acteursFilm);
    }

    public void setRealisateur(ActeursFilm realisateur) {
        this.realisateur = realisateur;
    }

    public void addVideo (VideoFilm videoFilm){
        this.videosFilms.add(videoFilm);
    }

    public void addCommentaire (Commentaire commentaire) {this.commentaires.add(commentaire);}

    public String getBudget() {
        return budget;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }
    public String getTitre_origine() {
        return titre_origine;
    }
    public String getResume() {
        return resume;
    }
    public String getDateSortie() {
        return dateSortie;
    }
    public String getPoster() {
        return poster;
    }

    /*public double getMoyenne() {
        return moyenne;
    }*/

    public ArrayList<ActeursFilm> getActeursFilms() {
        return acteursFilms;
    }

    public ArrayList<VideoFilm> getVideosFilms() {
        return videosFilms;
    }

    public ArrayList<Commentaire> getCommentaires() {
        return commentaires;
    }

    public ActeursFilm getRealisateur() {
        return realisateur;
    }
}
