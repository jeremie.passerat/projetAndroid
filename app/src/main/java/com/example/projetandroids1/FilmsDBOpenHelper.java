/**
 * Base de données qui va accueillir tous les films issus de l'api TMDB
 * Api qui sera appelée une seule fois à la première création du machin
 */

package com.example.projetandroids1;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

/**
 * Création de la BD et des 4 tables nécessaires pour stocker les films et leurs éléments
 * */

public class FilmsDBOpenHelper extends SQLiteOpenHelper {

    public static final String TABLE_FILMS="film";
    public static final String COLUMN_ID="id"; // utilisé pour les 3 DB
    public static final String COLUMN_TITRE="titre";
    public static final String COLUMN_TITREORI="titre_origine";
    public static final String COLUMN_BUDGET="budget";
    public static final String COLUMN_RESUME="resume";
    public static final String COLUMN_DATE="dateSortie";
    public static final String COLUMN_IMAGE="image";
    public static final String COLUMN_MOYENNE="moyenne";

    // Table pour les acteurs (avec l'id)
    public static final String TABLE_CASTING="casting";
    public static final String COLUMN_NOM="nom"; // utilisé pour le casting et les videos
    public static final String COLUMN_PERSO="personnage";
    public static final String COLUMN_JOB="job";

    // Table pour les videos (avec l'id et le nom)
    public static final String TABLE_VIDEOS="videos";
    public static final String COLUMN_URL="Url";

    // Table pour les commentaires (avec l'id)
    public static final String TABLE_COMMENTAIRES="commentaires";
    public static final String COLUMN_NOTE="note";
    public static final String COLUMN_DATEPOST="datePost";
    public static final String COLUMN_COM="commentaire";
    public static final String COLUMN_PSEUDO="pseudo";


    public FilmsDBOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public FilmsDBOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public FilmsDBOpenHelper(@Nullable Context context, @Nullable String name, int version, @NonNull SQLiteDatabase.OpenParams openParams) {
        super(context, name, version, openParams);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {


    String requeteFilms =
                "create table "+TABLE_FILMS+" ( "+
                COLUMN_ID + " integer primary key , " +
                COLUMN_TITRE + " text not null ," +
                COLUMN_TITREORI + " text not null ," +
                COLUMN_BUDGET + " text not null , " +
                COLUMN_RESUME + " text not null ," +
                COLUMN_DATE + " text not null ," +
                COLUMN_IMAGE + " text not null ," +
                COLUMN_MOYENNE + " real" +
                        ") ;";

        String requeteCasting =
                "create table "+TABLE_CASTING+" ( "+
                        COLUMN_ID + " integer , " +
                        COLUMN_NOM + " text not null ," +
                        COLUMN_PERSO + " text," +
                        COLUMN_IMAGE + " text," +
                        COLUMN_JOB + " text ," +
                        "PRIMARY KEY (" + COLUMN_ID + ", " + COLUMN_NOM + ")" +
                        ") ;";

        String requeteVideos =
                "create table "+TABLE_VIDEOS+" ( "+
                        COLUMN_ID + " integer , " +
                        COLUMN_NOM + " text not null ," +
                        COLUMN_URL + " text not null," +
                        "PRIMARY KEY (" + COLUMN_ID + ", " + COLUMN_NOM + ")" +
                        ") ;";

        String requeteCommentaires =
                "create table "+TABLE_COMMENTAIRES+" ( "+
                        COLUMN_ID + " integer , " +
                        COLUMN_NOTE + " integer ," +
                        COLUMN_DATEPOST + " text not null, " +
                        COLUMN_COM + " text not null, " +
                        COLUMN_PSEUDO + " text not null, " +
                        "PRIMARY KEY (" + COLUMN_ID + ", " + COLUMN_COM + ", " + COLUMN_PSEUDO + ")" +
                        ") ;";

        sqLiteDatabase.execSQL(requeteFilms);
        sqLiteDatabase.execSQL(requeteCasting);
        sqLiteDatabase.execSQL(requeteVideos);
        sqLiteDatabase.execSQL(requeteCommentaires);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // SAuf évolution de l'application cette base sera uniquement consultative
    }
}
