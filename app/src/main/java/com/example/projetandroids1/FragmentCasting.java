package com.example.projetandroids1;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Affichage d'un casting pour un film choisi
 */

public class FragmentCasting extends Fragment {

    ArrayList<ActeursFilm> casting;
    private TextView castingFilmSelect;
    private OnFragmentInteractionListener mListener;

    public FragmentCasting() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment FragmentCasting.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentCasting newInstance(ArrayList<ActeursFilm> casting) {
        FragmentCasting fragment = new FragmentCasting();
        Bundle args = new Bundle();
        args.putSerializable("casting", casting);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            casting = (ArrayList<ActeursFilm>) getArguments().getSerializable("casting");
        }

      //  Toast.makeText(getContext(), "Nombre d'acteurs : " + casting.size(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_fragment_casting, container, false);

        castingFilmSelect = view.findViewById(R.id.castingFilmSelect);
        StringBuilder castingTotal = new StringBuilder();

        int i;
        for (i = 0; i < casting.size(); i++){
            castingTotal.append(casting.get(i).getNom()).append(" - ").append(casting.get(i).getNomPerso() + "\n");
        }
       // Toast.makeText(getContext(), "Casting total " + castingTotal, Toast.LENGTH_SHORT).show();

        castingFilmSelect.setText(castingTotal.toString());

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
