package com.example.projetandroids1;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;


/**
 * Affichage des commentaires du film selectionné
 */

public class FragmentCommentaires extends Fragment {

    private Button newCom;

    ArrayList<Commentaire> commentaires;
    ListView listCom;

    int id;

    private OnFragmentInteractionListener mListener;

    public FragmentCommentaires() {
        // Required empty public constructor
    }

    public static FragmentCommentaires newInstance(ArrayList <Commentaire> commentaires, int id) {
        FragmentCommentaires fragment = new FragmentCommentaires();
        Bundle args = new Bundle();
        args.putSerializable("commentaires",commentaires);
        args.putInt("id", id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            commentaires = (ArrayList<Commentaire>) getArguments().getSerializable("commentaires");

            id = getArguments().getInt("id");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_fragment_commentaires, container, false);
        newCom = view.findViewById(R.id.newComm);

        listCom = view.findViewById(R.id.listeComs);

        //StringBuilder commTotal = new StringBuilder();

        if (commentaires == null){

            String[] resultat = new String[1];

            resultat[0] = "Aucun commentaire pour ce film";

            final ArrayAdapter<String> adapter;
            adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, resultat);

            ArrayList<String> listItems = new ArrayList<>(Arrays.asList(resultat));
            listCom.setAdapter(adapter);

        }
        else {

            String[] coms = new String[commentaires.size()];
            int j;
            for (j = 0; j < commentaires.size(); j++) {
                coms[j] = commentaires.get(j).getDate() + "\n" + commentaires.get(j).getAuteur() + " - note :" +
                        commentaires.get(j).getNote() + "\n" + commentaires.get(j).getContenu() ;
            }


            ArrayList<String> listItems = new ArrayList<>(Arrays.asList(coms));

            final ArrayAdapter<String> adapter;
            adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()), android.R.layout.simple_list_item_1, listItems);


            listCom.setAdapter(adapter);
        }

        newCom.setOnClickListener(view1 -> {

            Intent intent = new Intent(getActivity(), NouveauCom.class);
            intent.putExtra("id", id);
            startActivityForResult(intent, 3);

        });

        return view;
    }

    @Override
    public void onResume() {

        GetFilm getFilm = new GetFilm(Objects.requireNonNull(getContext()));
        Film film = getFilm.getFilmenBd(String.valueOf(id));
        commentaires = film.getCommentaires();

        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
