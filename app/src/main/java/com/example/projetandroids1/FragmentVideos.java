package com.example.projetandroids1;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * Affichage des vidéos pour un film sélectionné
 */
public class FragmentVideos extends Fragment  {
    ArrayList<VideoFilm> videos;

    ListView listVideos;


    private OnFragmentInteractionListener mListener;

    public FragmentVideos() {
        // Required empty public constructor
    }

    public static FragmentVideos newInstance(ArrayList<VideoFilm> trailers) {
        FragmentVideos fragment = new FragmentVideos();
        Bundle args = new Bundle();
        args.putSerializable("videos", trailers);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            videos = (ArrayList<VideoFilm>) getArguments().getSerializable("videos");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fragment_videos, container, false);

        listVideos = view.findViewById(R.id.listeVideos);

        if (videos.size() == 0) {
            String[] resultat = new String[1];

            resultat[0] = "Aucun vidéo pour ce film";

            final ArrayAdapter<String> adapter;
            adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, resultat);

            ArrayList<String> listItems = new ArrayList<>(Arrays.asList(resultat));
            listVideos.setAdapter(adapter);
        }
        else {

            String[] titres = new String[videos.size()];
            // On récupère seulement les titres
            int j;
            for (j = 0; j < videos.size(); j++) {
                titres[j] = videos.get(j).getNom();
            }

            ArrayList<String> listItems = new ArrayList<>(Arrays.asList(titres));

            final ArrayAdapter<String> adapter;
            adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()), android.R.layout.simple_list_item_1, listItems);


            listVideos.setAdapter(adapter);

            // La list view permettra par la suite de charger les vidéos
            listVideos.setOnItemClickListener((AdapterView<?> adapterView, View view1, int i, long l) -> {

                if (Objects.requireNonNull(adapter.getItem(i)).equals("Aucun vidéo pour ce film")) {

                } else {

                    String titre = videos.get(i).getUrl();

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(titre));
                    Objects.requireNonNull(getActivity()).startActivity(intent);
                }
            });
        }

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
