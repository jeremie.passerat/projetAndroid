package com.example.projetandroids1;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;

/**
    Permet de chercher dans la BD si un film existe (et de le charger le cas échéant)
 */

public class GetFilm {

    ArrayList<Film> mListeFilm;
    Context appContext;
    FilmsDBOpenHelper dbHelper;


    protected GetFilm(Context ctx) {

        mListeFilm = new ArrayList<>();
        // On s'assure de bien récupérer le contexte de l'app, même si on a récupéré un contexte d'ativity en paramètre
        appContext = ctx.getApplicationContext();

        // On va chercher la DB (creee si premier appel)
        dbHelper = new FilmsDBOpenHelper(appContext,"filmsdbNew",null,1);

    }

    // retourne le nombre de films présents avec cet id (1 ou 0 en gros)
    public int getFilm(String id){

        SQLiteDatabase db = this.dbHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT titre FROM film WHERE ID = ? ", new String[]{id});

        int nombre = cursor.getCount();

        cursor.close();
        return nombre;
    }

    // Recréée le film pour affichage
    public Film getFilmenBd(String id){

        SQLiteDatabase db = this.dbHelper.getReadableDatabase();

        // elements principaux du film
        Cursor cursor = db.rawQuery("SELECT * FROM film WHERE ID = ? ", new String[]{id});

        String titre = "";
         String titre_origine= "";
         String budget= "";
         String resume= "";
         String dateSortie= "";
         String poster= "";
         double moyenne = 0.0;

         while (cursor.moveToNext()) {
             titre = cursor.getString(1);
             titre_origine = cursor.getString(2);
             budget = cursor.getString(3);
             resume = cursor.getString(4);
             dateSortie = cursor.getString(5);
             poster = cursor.getString(6);
             moyenne = cursor.getDouble(7);
         }

         Film film = new Film(Integer.parseInt(id), titre, titre_origine, budget, resume, dateSortie, poster, moyenne);

        // On charge les acteurs
         cursor = db.rawQuery("SELECT * FROM casting WHERE ID = ? ", new String[]{id});


         String nomActeur;
         String nomPerso;
         String imageActeur;
         String job;

        while (cursor.moveToNext()) {

            nomActeur = cursor.getString(1);
            nomPerso = cursor.getString(2);
            imageActeur = cursor.getString(3);
            job = cursor.getString(4);

            if (job != null && job.equals("Director")){
                film.setRealisateur(new ActeursFilm(job, Integer.parseInt(id), nomActeur, imageActeur));
            }
            else
                film.addCasting(new ActeursFilm(Integer.parseInt(id), nomActeur, nomPerso, imageActeur));
        }

        // On charge les vidéos
        cursor = db.rawQuery("SELECT * FROM videos WHERE ID = ? ", new String[]{id});

        String nomVideo;
        String Url;

        while (cursor.moveToNext()) {

            nomVideo = cursor.getString(1);
            Url = cursor.getString(2);


            film.addVideo(new VideoFilm(Integer.parseInt(id), nomVideo, Url));
        }

        // On charge les commentaires
        cursor = db.rawQuery("SELECT * FROM commentaires WHERE ID = ? ", new String[]{id});

        int note;
        String date;
        String contenu;
        String auteur;

        while (cursor.moveToNext()) {

            note = cursor.getInt(1);
            date = cursor.getString(2);
            contenu = cursor.getString(3);
            auteur = cursor.getString(4);

            film.addCommentaire(new Commentaire(Integer.parseInt(id), contenu, auteur, note, date));
        }

        cursor.close();
        db.close();

        return film;
    }
}