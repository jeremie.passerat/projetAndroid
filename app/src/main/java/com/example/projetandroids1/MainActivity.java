package com.example.projetandroids1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements FragmentResume.OnFragmentInteractionListener, FragmentCasting.OnFragmentInteractionListener, FragmentCommentaires.OnFragmentInteractionListener, FragmentVideos.OnFragmentInteractionListener, AffichageFilmFragment.OnFragmentInteractionListener, DetailFilmFragment.OnFragmentInteractionListener  {

    AffichageFilmFragment affichageFilmFragment;
    DetailFilmFragment detailFilmFragment;
    EditText editText;
    TextView textView;
    FragmentTransaction transaction;
    Pseudo singlPseudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rechangeFragment();
        editText = findViewById(R.id.valueFilm);
        Intent intent = getIntent();
        textView = findViewById(R.id.affichage);

        //Toast.makeText(this, "TW3 > Skyrim", Toast.LENGTH_SHORT).show();
        // Mise en place du singleton pour le pseudo
        singlPseudo = Pseudo.getInstance();
        singlPseudo.setPseudo(intent.getStringExtra("pseudo"));

        textView.setText("Bienvenue " + singlPseudo.getPseudo() + " - Rechercher un film");
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    // Permet de récupérer tous les résultats pertinents en fonction de la recherche
    public void rechercherFilm(View view) {

        // On masque le clavier virtuel
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        Fragment Current = getSupportFragmentManager().findFragmentById(R.id.mainAffichageFilm);

        if (editText.getText().length() == 0) {
            Toast.makeText(this, "Erreur, merci de renseigner un nom de film", Toast.LENGTH_SHORT).show();
        }

        else {
            if (Current instanceof DetailFilmFragment) {
                rechangeFragment();

                // On appelle l'activité de recherche des films
                Intent intent = new Intent(this,ChargerFilmsNew.class);
                intent.putExtra("Recherche", editText.getText().toString());
                startActivityForResult(intent, 1);
                editText.setText("");

            } else {

                Intent intent = new Intent(this,ChargerFilmsNew.class);
                intent.putExtra("Recherche", editText.getText().toString());
                startActivityForResult(intent, 1);
                editText.setText("");
            }
        }


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {

            String[] films = data.getStringArrayExtra("resultat");

            assert films != null;
            affichageFilmFragment.showFilm(films);
        }

        if (requestCode == 2) {

            Film film = (Film) data.getSerializableExtra("film");

            transaction = getSupportFragmentManager().beginTransaction();
            detailFilmFragment = new DetailFilmFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable("film", film);
            detailFilmFragment.setArguments(bundle);
            transaction.replace(R.id.mainAffichageFilm, detailFilmFragment);
            transaction.commitAllowingStateLoss();
            getSupportFragmentManager().executePendingTransactions();

        }
        else {

            super.onActivityResult(requestCode,resultCode,data);
            String idFilm = data.getStringExtra("idFilm");

            // Ne se déclenche que s'il y a un nouveau commentaire
            if (idFilm == null){

            }
            else {
                GetFilm getFilm = new GetFilm(this);
                Film film = getFilm.getFilmenBd(idFilm);

                transaction = getSupportFragmentManager().beginTransaction();
                detailFilmFragment = new DetailFilmFragment();

                Bundle bundle = new Bundle();
                bundle.putSerializable("film", film);
                detailFilmFragment.setArguments(bundle);
                transaction.replace(R.id.mainAffichageFilm, detailFilmFragment);
                transaction.commitAllowingStateLoss();
                getSupportFragmentManager().executePendingTransactions();
            }
        }
    }

    //permet de recréer la vue (si on laisse l'app en arrière plan par exemple
    @Override
    public void recreate()
    {
        super.recreate();
    }

    // permet de changer de vue une fois un film de la liste choisi
    public void changeFragment(String idFilm){

        GetFilm getFilm = new GetFilm(getApplicationContext());
        int nbFilmenBd = getFilm.getFilm(idFilm);

        if (nbFilmenBd > 0){

            Film film= getFilm.getFilmenBd(idFilm);
            transaction = getSupportFragmentManager().beginTransaction();
            detailFilmFragment = new DetailFilmFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable("film", film);

            detailFilmFragment.setArguments(bundle);
            transaction.replace(R.id.mainAffichageFilm, detailFilmFragment);
            transaction.commitAllowingStateLoss();
            getSupportFragmentManager().executePendingTransactions();
        }
        else {

            // Charger les informations du film choisi (en allant les chercher sur TMDB)
            Intent intent = new Intent(this, ChargerFilmChoisi.class);
            intent.putExtra("IdFilm", idFilm);
            startActivityForResult(intent, 2);
        }

    }


    // Permet de réafficher le fragment d'affichage des résultats de recherche
    public void rechangeFragment(){
        transaction = getSupportFragmentManager().beginTransaction();
        affichageFilmFragment = new AffichageFilmFragment();
        transaction.replace(R.id.mainAffichageFilm, affichageFilmFragment);
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();

    }
}

