package com.example.projetandroids1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/**
 * Pour rechanger son pseudo (ou le mettre en place la premièrez fois)
 */

public class NewPseudo extends AppCompatActivity {

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editText = findViewById(R.id.pseudo);
    }

    public void chargerApp(View view) {
        setRecordedPseudo(editText.getText().toString());

        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra("pseudo", editText.getText().toString());
        startActivity(intent);
        finish();
    }

    public void setRecordedPseudo(String newPW) {
        try {
            FileOutputStream fileOutput = this.openFileOutput("pseudo",MODE_PRIVATE);
            PrintWriter writer = new PrintWriter(fileOutput);
            writer.print(newPW);
            writer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
