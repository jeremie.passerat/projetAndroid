package com.example.projetandroids1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NouveauCom extends AppCompatActivity implements DetailFilmFragment.OnFragmentInteractionListener {

    EditText commentaire;
    NumberPicker note;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nouveau_com);
        commentaire = findViewById(R.id.zoneCommentaire);
        note = findViewById(R.id.note);

        id = getIntent().getIntExtra("id", 0);


        note.setMinValue(1);
        note.setMaxValue(10);

        note.setOnValueChangedListener((numberPicker, i, i1) -> note.setTag(i1));

    }

    public void envoiCommentaire(View view){

        Pseudo pseudo = Pseudo.getInstance();

       /* Toast.makeText(this, "Auteur :" + pseudo.getPseudo(), Toast.LENGTH_SHORT).show();
        Toast.makeText(this, commentaire.getText(), Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "note :" + note.getValue(), Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "id Film : " + id, Toast.LENGTH_SHORT).show();
*/

        FilmsDBOpenHelper dbHelper = new FilmsDBOpenHelper(this, "filmsdbNew", null, 1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues toAdd = new ContentValues();

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        Date date = new Date(System.currentTimeMillis());

        toAdd.put(FilmsDBOpenHelper.COLUMN_ID, id);
        toAdd.put(FilmsDBOpenHelper.COLUMN_NOTE, note.getValue());
        toAdd.put(FilmsDBOpenHelper.COLUMN_DATEPOST, formatter.format(date));
        toAdd.put(FilmsDBOpenHelper.COLUMN_COM, commentaire.getText().toString());
        toAdd.put(FilmsDBOpenHelper.COLUMN_PSEUDO, pseudo.getPseudo());


        db.insert(FilmsDBOpenHelper.TABLE_COMMENTAIRES, null, toAdd);
        db.close();


        // On repasse sur l'appli
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("idFilm", String.valueOf(id));

        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
