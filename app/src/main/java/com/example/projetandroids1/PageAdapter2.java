package com.example.projetandroids1;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class PageAdapter2 extends FragmentStatePagerAdapter {

    private List<Fragment> fragments;

    //Default Constructor
    public PageAdapter2(FragmentManager mgr, List<Fragment> fragments1) {
        super(mgr);
        this.fragments = fragments1;
    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


    @Override
    public int getCount() {
        return(fragments.size()); //Number of page to show
    }


    @Override
    public CharSequence getPageTitle(int position) {

        if (position == 0)
            return "Résumé";
        else if (position == 1)
            return "Casting";
        else if (position == 2)
            return "Vidéos";
        else
            return "Commentaires";

    }



    @Override
    public Fragment getItem(int position) {

        return fragments.get(position);
    }




}
