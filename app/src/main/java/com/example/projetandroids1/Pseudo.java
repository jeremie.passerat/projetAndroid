package com.example.projetandroids1;

/**
 * Singleton pour garder le pseudo
 */

public class Pseudo {

    private static Pseudo mInstance= null;

    public String pseudo;

    protected Pseudo(){}

    public static synchronized Pseudo getInstance() {
        if(null == mInstance){
            mInstance = new Pseudo();
        }
        return mInstance;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPseudo() {
        return pseudo;
    }
}

