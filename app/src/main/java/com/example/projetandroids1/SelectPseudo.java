package com.example.projetandroids1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Choix du pseudo
 */

public class SelectPseudo extends AppCompatActivity {

    ListView listView;
    String pseudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_pseudo);
        listView = findViewById(R.id.choixDepart);

        pseudo = getRecordedPseudo();
        if (pseudo != null) {

            /* On offre un choix à l'utilisateur :
                - Garder le pseudo en mémoire
                - Le rechanger
             */

            ArrayList<String> listItems = new ArrayList<>();
            listItems.add("Se connecter sur le pseudo "+ pseudo);
            listItems.add("Choisir un nouveau pseudo");

            final ArrayAdapter<String> adapter;
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listItems);

            listView.setAdapter(adapter);


            listView.setOnItemClickListener((AdapterView<?> adapterView, View view1, int i, long l) -> {
                    // On garde le pseudo
                     if (i == 0){
                         Intent intent = new Intent(this,MainActivity.class);
                         intent.putExtra("pseudo", pseudo);
                         startActivity(intent);
                         finish();
                     }
                     // On change le pseudo
                     else {
                         Intent intent = new Intent(this, NewPseudo.class);
                         startActivity(intent);
                     }
            });
        }else {
            // Pseudo inconnu en mémoire, on en créée un
            Intent intent = new Intent(this,NewPseudo.class);
            startActivity(intent);
            finish();
        }
    }

    public String getRecordedPseudo() {
        try {
            FileInputStream fileInput = this.openFileInput("pseudo");
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileInput));
            String pw = reader.readLine();
            pw = pw.trim();
            reader.close();
            return pw;
        } catch (FileNotFoundException e) {
            // Cas où on n'avait pas défini de pseudo. On renvoie null pour signifier cet etat de fait
            return null;
        } catch (IOException e) {
            //TODO crasher proprement
            e.printStackTrace();
            return null;
        }
    }


}
