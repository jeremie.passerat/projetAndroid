package com.example.projetandroids1;


/*
    Une vidéo liée à un film
 */

import java.io.Serializable;

public class VideoFilm implements Serializable {

    private int id;
    private String nom;
    private String Url;

    public VideoFilm (int id, String name, String Url) {
        this.id = id;
        this.nom = name;
        this.Url = Url;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getUrl() {
        return Url;
    }
}
